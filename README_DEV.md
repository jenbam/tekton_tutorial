# Tekton
## Requirements


# aws
## [EC2 configuration](https://minikube.sigs.k8s.io/docs/start/)

| AMI |Amazon Linux|
| ---     |            ---  |
| Instance Type |t2.medium (2 vCPU, 4GB Memory)|
|Storage   | 	20 GB (gp2)                     |
|Tags	   |  Key=name, Value=minikube         |
|Security Group|[Name = minikube-sg], [SSH, 0.0.0.0/0]|
|Key Pair |	Create your own keypair.You will need this to SSH to your EC2 Instance |
---

### Connect to ec2 Instance

```bash
 ssh -i " ~/Documents/ACloudGuru/aws-jbamanya/LondonBeat.cer" ec2-user@XXXXXXXXX.compute-1.amazonaws.com

```

## Install kubectl

```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x ./kubectl

sudo mv ./kubectl /usr/local/bin/kubectl

```

# Install Git
```bash
sudo yum install git -y
```
## Installation of Docker on EC2
```bash
sudo yum install docker -y
#start service if necessary
sudo systemctl start docker
#Check ownership for the Docker Unix socket
sudo ls -la /var/run/docker.sock
sudo chown ec2-user:docker /var/run/docker.sock
sudo usermod -aG docker ec2-user
#check service status
sudo service docker status
```

## [Installation of Minikube on EC2](https://minikube.sigs.k8s.io/docs/start/)

```bash
  sudo yum update -y

  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
  
  # check minikube version
  minikube version
 

```


## Start cluster

```bash
minikube start
```

## [Install tkn](https://github.com/tektoncd/cli)
```bash
# Get the tar.xz
curl -LO https://github.com/tektoncd/cli/releases/download/v0.21.0/tkn_0.21.0_Linux_x86_64.tar.gz
# Extract tkn to your PATH (e.g. /usr/local/bin)
sudo tar xvzf tkn_0.21.0_Linux_x86_64.tar.gz -C /usr/local/bin/ tkn
```

## Clone Git Repo
```bash
git clone https://Jennix2016@bitbucket.org/jenbam/tekton_tutorial.git
```

## TODO
- automate above steps


## References
- [Docker configuration](https://phoenixnap.com/kb/cannot-connect-to-the-docker-daemon-error)

- [Tekton](https://tekton.dev/docs/getting-started/)

- [Minikube](https://v1-18.docs.kubernetes.io/docs/setup/learning-environment/minikube/)
